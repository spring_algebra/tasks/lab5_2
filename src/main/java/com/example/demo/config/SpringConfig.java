/*
 * Algebra labs.
 */
 
package com.example.demo.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Import;

// Declare as a Spring configuration class
// and import other configuration classes
@Configuration
@EnableAspectJAutoProxy
@Import({SpringAspectsConfig.class, SpringRepositoryConfig.class, SpringServicesConfig.class}) 
public class SpringConfig {}