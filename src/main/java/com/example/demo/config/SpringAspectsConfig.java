/*
 * Algebra labs.
 */
 
package com.example.demo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.example.demo.aspects.LogAspect;

// Declare as a Spring configuration class
@Configuration
public class SpringAspectsConfig {
	
	// TODO: Declare LogAspect as a bean

}